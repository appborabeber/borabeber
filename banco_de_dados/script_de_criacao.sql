
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 01/04/2016 às 17:14:33
-- Versão do Servidor: 10.0.20-MariaDB-log
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u467999451_borab`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_endereco`
--

CREATE TABLE IF NOT EXISTS `tbl_endereco` (
  `id_endereco` int(11) NOT NULL AUTO_INCREMENT,
  `desc_endereco` text COLLATE utf8_unicode_ci,
  `num_coordenadas` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` int(11) DEFAULT NULL,
  `tbl_estabelecimento_id_estabelecimento` int(11) NOT NULL,
  PRIMARY KEY (`id_endereco`),
  KEY `fk_tbl_endereco_tbl_estabelecimento1_idx` (`tbl_estabelecimento_id_estabelecimento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_estabelecimento`
--

CREATE TABLE IF NOT EXISTS `tbl_estabelecimento` (
  `id_estabelecimento` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_plano_id_plano` int(11) NOT NULL,
  `tbl_usuario_id_usuario` int(11) NOT NULL,
  `nom_estabelecimento` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_cpf_cnpj` int(11) DEFAULT NULL,
  `img_estabelecimento` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'imagem do estabelecimento.',
  `lo_ativo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'cadastro ativo. S = sim; N = nao;',
  PRIMARY KEY (`id_estabelecimento`),
  KEY `fk_tbl_estabelecimento_tbl_plano1_idx` (`tbl_plano_id_plano`),
  KEY `fk_tbl_estabelecimento_tbl_usuario1_idx` (`tbl_usuario_id_usuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_marca`
--

CREATE TABLE IF NOT EXISTS `tbl_marca` (
  `id_marca` int(11) NOT NULL AUTO_INCREMENT COMMENT 'cadastra as marcas das bebidas.',
  `nom_marca` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ex.: skol',
  PRIMARY KEY (`id_marca`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `tbl_marca`
--

INSERT INTO `tbl_marca` (`id_marca`, `nom_marca`) VALUES
(1, 'Skol'),
(2, 'Brahma'),
(3, 'Antarctica'),
(4, 'Budweiser');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_perfil`
--

CREATE TABLE IF NOT EXISTS `tbl_perfil` (
  `id_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `nome_perfil` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_perfil` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_perfil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_plano`
--

CREATE TABLE IF NOT EXISTS `tbl_plano` (
  `id_plano` int(11) NOT NULL AUTO_INCREMENT,
  `desc_plano` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom_plano` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_preco_vigente` float DEFAULT NULL,
  `num_preco_antigo` float DEFAULT NULL,
  `lo_ativo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'cadastro ativo. S = sim; N = nao;',
  PRIMARY KEY (`id_plano`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_telefone`
--

CREATE TABLE IF NOT EXISTS `tbl_telefone` (
  `id_telefone` int(11) NOT NULL AUTO_INCREMENT,
  `num_telefone` int(11) DEFAULT NULL,
  `lo_ativo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tbl_endereco_id_endereco` int(11) NOT NULL,
  PRIMARY KEY (`id_telefone`),
  KEY `fk_tbl_telefone_tbl_endereco1_idx` (`tbl_endereco_id_endereco`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipo_bebida`
--

CREATE TABLE IF NOT EXISTS `tbl_tipo_bebida` (
  `id_tipo_bebida` int(11) NOT NULL AUTO_INCREMENT,
  `nom_tipo_bebida` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ex.: cerveja, uísque, tequila.',
  PRIMARY KEY (`id_tipo_bebida`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tbl_tipo_bebida`
--

INSERT INTO `tbl_tipo_bebida` (`id_tipo_bebida`, `nom_tipo_bebida`) VALUES
(1, 'Cerveja'),
(2, 'Refrigerante'),
(3, 'Vodka');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipo_bebida_has_tbl_marca`
--

CREATE TABLE IF NOT EXISTS `tbl_tipo_bebida_has_tbl_marca` (
  `id_bebida` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_tipo_bebida_id_tipo_bebida` int(11) NOT NULL,
  `tbl_marca_id_marca` int(11) NOT NULL,
  `titulo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ex.: Lata, Long Neck, Garrafa.',
  `subtitulo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` text COLLATE utf8_unicode_ci COMMENT 'Menorzinha, mais fininha e avisa quando a sua redondinha tá geladinha. Quer mais? Piscou, gelou',
  `tamanho` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ex.: 269 ml',
  `img_bebida` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'imagem da bebida. ex.: img da lata, img da garrafa.',
  `lo_ativo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'cadastro ativo. S = sim; N = nao;',
  PRIMARY KEY (`id_bebida`),
  KEY `fk_tbl_tipo_bebida_has_tbl_marca_tbl_marca1_idx` (`tbl_marca_id_marca`),
  KEY `fk_tbl_tipo_bebida_has_tbl_marca_tbl_tipo_bebida1_idx` (`tbl_tipo_bebida_id_tipo_bebida`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipo_bebida_has_tbl_marca_has_tbl_estabelecimento`
--

CREATE TABLE IF NOT EXISTS `tbl_tipo_bebida_has_tbl_marca_has_tbl_estabelecimento` (
  `tbl_tipo_bebida_has_tbl_marca_id_bebida` int(11) NOT NULL,
  `tbl_estabelecimento_id_estabelecimento` int(11) NOT NULL,
  `dt_cadastro` datetime DEFAULT NULL COMMENT 'data do cadastro.',
  `dt_inicio` datetime DEFAULT NULL COMMENT 'quando se inicia a visualização do anuncio.',
  `dt_fim` datetime DEFAULT NULL COMMENT 'quando acaba a visualização do anuncio.',
  `num_preco` float DEFAULT NULL COMMENT 'preço a bebida. ex.: 2,50, 6,99...',
  `lo_ativo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'cadastro ativo. S = sim; N = nao;',
  PRIMARY KEY (`tbl_tipo_bebida_has_tbl_marca_id_bebida`,`tbl_estabelecimento_id_estabelecimento`),
  KEY `fk_tbl_tipo_bebida_has_tbl_marca_has_tbl_estabelecimento_tb_idx` (`tbl_estabelecimento_id_estabelecimento`),
  KEY `fk_tbl_tipo_bebida_has_tbl_marca_has_tbl_estabelecimento_tb_idx1` (`tbl_tipo_bebida_has_tbl_marca_id_bebida`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nom_usuario` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_cpf` int(11) DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_usuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dt_cadastro` datetime DEFAULT NULL,
  `lo_ativo` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 's= sim; n=nao;',
  `tbl_perfil_id_perfil` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_tbl_usuario_tbl_perfil1_idx` (`tbl_perfil_id_perfil`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
