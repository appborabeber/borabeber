<?php
header ("Access-Control-Allow-Origin: *");
header ("Access-Control-Allow-Methods: POST, OPTIONS, GET, DELETE");
header ("Access-Control-Allow-Headers: Authorization, origin, x-requested-with, Content-Type, Accept");

// INICIO CONFIGURAÇÃO DO CROS-ORIGIN
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE");         
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);
}
// FIM CONFIGURAÇÃO DO CROS-ORIGIN
 
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/', function () {
  echo "SlimProdutos";
});

//BEBIDA

$app->get('/buscarDadosFipe','buscarDadosTabelaFipe');

// $app->get('/bebida','buscarTipoBebida');
// $app->post('/insertBebida', 'novaBebida');


// //TIPO DE BEBIDA
// $app->get('/tipoBebida','buscaTipoBebida');
// $app->get('/tipoBebida/:id','buscaTipoBebidaPorId');
// $app->post('/insertTipoBebida', 'novoTipoBebida');
// $app->post('/updateTipoBebida/:id', 'alterarTipoBebida');
// $app->delete('/deleteTipoBebida/:id','deletarTipoBebida');


// //MARCA
// $app->get('/marca','buscarMarcas');
// $app->get('/marca/:id','buscaMarcaPorId');
// $app->post('/insertMarca', 'novaMarca');
// $app->post('/updateMarca/:id', 'alterarMarca');
// $app->delete('/deleteMarca/:id','deletarMarca');


$app->run();

function getConn(){
  return new PDO('mysql:host=mysql.hostinger.com.br;dbname=u133898663_fipe', 'u133898663_fipe', 'wsfipe2016');
}


//FUNCOES PARA BEBIDA

function buscarDadosTabelaFipe(){
  $stmt = getConn()->query("SELECT 
    fp_marca.marca,
    fp_modelo.modelo,
    fp_modelo.codigo_fipe as cod_fipe,
    fp_ano.ano,
    fp_ano.combustivel,
    fp_ano.valor
  FROM fp_marca
  INNER JOIN  fp_modelo ON  fp_marca.codigo_marca = fp_modelo.codigo_marca
  INNER JOIN  fp_ano ON fp_modelo.codigo_modelo = fp_ano.codigo_modelo ORDER BY fp_marca.marca, fp_ano.ano, fp_modelo.modelo DESC");  
  $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);
  echo json_encode($categorias);
}


function novaBebida(){
  $request = \Slim\Slim::getInstance()->request();
  $item = json_decode($request->getBody());
  $sql = "INSERT INTO
            tbl_tipo_bebida_has_tbl_marca
                (
                    tbl_tipo_bebida_id_tipo_bebida,
                    tbl_marca_id_marca,
                    titulo,
                    subtitulo,
                    descricao,
                    tamanho,
                    img_bebida,
                    lo_ativo
                )
                values
                (
                    :id_tipo_bebida,
                    :marca,
                    :titulo,
                    :subtitulo,
                    :descricao,
                    :tamanho,
                    'teste.jpg',
                    's'
                )";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("id_tipo_bebida", $item->id_tipo_bebida);
  $stmt->bindParam("id_marca", $item->id_marca);
  $stmt->bindParam("titulo", $item->titulo);
  $stmt->bindParam("subtitulo", $item->subtitulo);
  $stmt->bindParam("descricao", $item->descricao);
  $stmt->bindParam("tamanho", $item->tamanho);

  $stmt->execute();

  echo json_encode($item);
}
//FIM BEBIDA

//FUNCOES PARA MARCAS
function buscarMarcas(){
  $stmt = getConn()->query("SELECT * FROM tbl_marca order by nom_marca ASC");
  $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);
  echo json_encode($categorias);
}

function buscaMarcaPorId($id){
  $request = \Slim\Slim::getInstance()->request();
  $sql = "SELECT * FROM tbl_marca WHERE id_marca  = :idMarca";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("idMarca", $id);
  $stmt->execute();
  $item = $stmt->fetchObject(); 

  echo json_encode($item);
}

function novaMarca(){
  $request = \Slim\Slim::getInstance()->request();
  $item = json_decode($request->getBody());
  $sql = "INSERT INTO tbl_marca (nom_marca) values (:marca)";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("marca", $item->nome);
  $stmt->execute();

  echo json_encode($item);
}

function alterarMarca($id){
  $request = \Slim\Slim::getInstance()->request();
  $item = json_decode($request->getBody());
  $sql = "UPDATE tbl_marca SET nom_marca = :marca WHERE id_marca = :idMarca";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("marca", $item->nome);
  $stmt->bindParam("idMarca", $id);
  $stmt->execute();

  echo json_encode($item);
}

function deletarMarca($id){
  $sql = "DELETE FROM tbl_marca WHERE id_marca = :idMarca";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("idMarca", $id);
  $stmt->execute();

  echo json_encode($item);
}
//FIM


//FUNCOES PARA TIPO DE BEBIDA

function buscaTipoBebida(){
  $stmt = getConn()->query("SELECT * FROM tbl_tipo_bebida order by nom_tipo_bebida ASC");
  $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);
  echo json_encode($categorias);
}

function buscaTipoBebidaPorId($id){
  $request = \Slim\Slim::getInstance()->request();
  $sql = "SELECT * FROM tbl_tipo_bebida WHERE id_tipo_bebida  = :idTipoBebida";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("idTipoBebida", $id);
  $stmt->execute();
  $item = $stmt->fetchObject(); 

  echo json_encode($item);
}

function novoTipoBebida(){
  $request = \Slim\Slim::getInstance()->request();
  $item = json_decode($request->getBody());
  $sql = "INSERT INTO tbl_tipo_bebida (nom_tipo_bebida) values (:tipoBebida)";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("tipoBebida", $item->nome);
  $stmt->execute();

  echo json_encode($item);
}

function alterarTipoBebida($id){
  $request = \Slim\Slim::getInstance()->request();
  $item = json_decode($request->getBody());
  $sql = "UPDATE tbl_tipo_bebida SET nom_tipo_bebida = :tipoBebida WHERE id_tipo_bebida = :idTipoBebida";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("tipoBebida", $item->nome);
  $stmt->bindParam("idTipoBebida", $id);
  $stmt->execute();

  echo json_encode($item);
}

function deletarTipoBebida($id){
  $sql = "DELETE FROM tbl_tipo_bebida WHERE id_tipo_bebida = :idTipoBebida";
  $conn = getConn();
  $stmt = $conn->prepare($sql);
  $stmt->bindParam("idTipoBebida", $id);
  $stmt->execute();

  echo json_encode($item);
}
//FIM TIPO BEBIDA