angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'firebase', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    };

    if (window.StatusBar) {
      StatusBar.styleDefault();
    };

    // Configuração do FIREBASE
    var config = {
      apiKey: "AIzaSyDtjN3MD-CzqGPNzCXlgPLpnjRqb36Wjvw",
      authDomain: "app-borabeber.firebaseapp.com",
      databaseURL: "https://app-borabeber.firebaseio.com",
      storageBucket: "app-borabeber.appspot.com",
    };
    firebase.initializeApp(config);
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

    .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'templates/menu.html'
  })

  .state('menu.ofertas', {
    url: '/ofertas',
    views: {
      'menu-ofertas': {
        templateUrl: 'templates/tab-ofertas.html',
        controller: 'OfertasCtrl'
      }
    }
  })

  .state('menu.locais', {
    url: '/locais',
    views: {
      'menu-locais': {
        templateUrl: 'templates/tab-locais.html',
        controller: 'LocaisCtrl'
      }
    }
  })

  .state('menu.favoritos', {
    url: '/favoritos',
    views: {
      'menu-favoritos': {
        templateUrl: 'templates/tab-favoritos.html',
        controller: 'FavoritosCtrl'
      }
    }
  })

  .state('menu.admin', {
    url: '/admin',
    views: {
      'menu-admin': {
        templateUrl: 'templates/tab-admin.html',
        controller: 'AdminController'
      }
    }
  });

  $urlRouterProvider.otherwise('/menu/ofertas');

});
