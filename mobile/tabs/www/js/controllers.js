angular.module('starter.controllers', [])

.controller('OfertasCtrl', function($scope, $rootScope, $firebaseArray, FirebaseServico) {

  var ref = firebase.database().ref().child("marca");
  var obj = $firebaseArray(ref);

  obj.$loaded().then(function() {
    angular.forEach(obj, function(item) {
      console.log(item);
    })
  });

})

.controller('LocaisCtrl', function($scope, $rootScope, Chats, $cordovaGeolocation) {

  if (window.plugin) {

    // CRIA O GOOGLE MAPS COM AS CONFIGURAÇÕES DE ANIMAÇÃO E MARCAÇÃO
    var map = window.plugin.google.maps.Map.getMap();
    map.addEventListener(window.plugin.google.maps.event.MAP_READY, function onMapInit(map) {
      var mapDiv = window.document.getElementById("map_canvas");
      map.setDiv(mapDiv);
      map.setOptions({
        'mapType': plugin.google.maps.MapTypeId.ROADMAP,
        'controls': {
          'compass': true,
          'myLocationButton': true
        }
      });
    });

    // PEGA A LOCALIZAÇÃO ATUAL
    var posOptions = {
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 0
    };

    $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position) {
      var localizaoAtual = new plugin.google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      // ADICIONA ANIMAÇÃO DE ENTRADA
      map.animateCamera({
        'target': localizaoAtual,
        'zoom': 17,
        'tilt': 30,
        'bearing': 140,
        'duration': 3000 // 3 segundos
      });

      // ADICIONA MARCADOR
      // map.addMarker({
      //   'position': localizaoAtual
      // });

    }, function(erro) {
      console.log(erro);
    });
  };
})

.controller('FavoritosCtrl', function($scope) {

  $scope.settings = {
    enableFriends: true
  };

})


.controller('AdminController', function($scope) {

  // "marca": {},
  // "tipo_bebida": {},
  // "bebida": {},
  // "endereco": {},
  // "estabelecimento": {},
  // "telefone": {},
  // "perfil": {},
  // "plano": {},
  // "usuario": {}

  // Função que Gera uma Chave e retorna o Caminho a ser Salvo
  var geraChave = function(tabela) {
    var chave = firebase.database().ref().child(tabela).push().key;
    tabelaComChave = tabela + "/" + chave;
    return tabelaComChave
  }

  var databaseMarca = function() {
    firebase.database().ref(geraChave("marca")).set({
      nome: "Bohemia"
    });
    firebase.database().ref(geraChave("marca")).set({
      nome: "Skol"
    });
    firebase.database().ref(geraChave("marca")).set({
      nome: "Budweiser"
    });
    firebase.database().ref(geraChave("marca")).set({
      nome: "Antarctica"
    });
  }

  var databaseTipoBebida = function() {
    firebase.database().ref(geraChave("tipo_bebida")).set({
      nome: "Cerveja"
    });
    firebase.database().ref(geraChave("tipo_bebida")).set({
      nome: "Wisky"
    });
    firebase.database().ref(geraChave("tipo_bebida")).set({
      nome: "Vodka"
    });
  }

  var databaseBebida = function() {
    firebase.database().ref(geraChave("bebida")).set({
      nome: "Cerveja"
    });
    firebase.database().ref(geraChave("bebida")).set({
      nome: "Wisky"
    });
    firebase.database().ref(geraChave("bebida")).set({
      nome: "Vodka"
    });
  }

  databaseMarca();



  // var chave = firebase.database().ref().child('usuarios/').push().key;
  // firebase.database().ref('usuario/' + chave).set({
  //   nome: "Arnaldo Coelho",
  //   email: "arnaldocoelho22@gmail.com"
  // });

  // var chave = firebase.database().ref().child('usuarios/').push().key;
  // firebase.database().ref('usuario/' + chave).set({
  //   nome: "Allison",
  //   email: "allison87@gmail.com"
  // });

  // Faz a leitura a cada mudança do Banco
  // firebase.database().ref('usuario/').on('value', function(item) {
  //   console.log(item.val());
  // });

  // firebase.database().ref('usuario').once('value', function(item) {
  //   angular.forEach(item.val(), function(valor){
  //     console.log(valor);
  //   });
  // });

  // var provider = new firebase.auth.GoogleAuthProvider();
  // firebase.auth().signInWithRedirect(provider);
  // firebase.auth().getRedirectResult().then(function(result) {
  //   if (result.credential) {
  //       var token = result.credential.accessToken;
  //     }
  //     var user = result.user;
  //   }).catch(function(error) {
  //     var errorCode = error.code;
  //     var errorMessage = error.message;
  //     var email = error.email;
  //     var credential = error.credential;
  //   });
});
