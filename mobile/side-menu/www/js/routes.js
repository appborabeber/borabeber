angular.module('app.routes', []).config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

    .state('menu', {
      url: '/menu',
      abstract:true,
      templateUrl: 'templates/menu/menu.html'
    })

    .state('menu.bebida', {
      url: '/bebida',
      views: {
        'menu-usuario': {
          templateUrl: 'templates/bebida.html',
          controller: 'BebidaCtrl'
        }
      }
    })

    .state('menu.marcaBebida', {
      url: '/marcaBebida',
      views: {
        'menu-usuario': {
          templateUrl: 'templates/marcaBebida.html',
          controller: 'MarcaBebidaCtrl'
        }
      }
    })

    .state('menu.tipoBebida', {
      url: '/tipoBebida',
      views: {
        'menu-usuario': {
          templateUrl: 'templates/tipoBebida.html',
          controller: 'TipoBebidaCtrl'
        }
      }
    })

    .state('menu.ofertas', {
      url: '/ofertas',
      views: {
        'menu-usuario': {
          templateUrl: 'templates/ofertas.html',
          controller: 'OfertasCtrl'
        }
      }
    })

    .state('menu.comerciante', {
      url: '/comerciante',
      views: {
        'menu-usuario': {
          templateUrl: 'templates/comerciante.html'
        }
      }
    })

    .state('menu.mapa', {
      url: '/mapa',
      views: {
        'menu-usuario': {
          templateUrl: 'templates/mapa.html',
          controller: 'MapaCtrl'
        }
      }
    })
    ;

  $urlRouterProvider.otherwise('/menu/ofertas');

});
