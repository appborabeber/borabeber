angular.module('app.controllers', [])

  /*
   * DATA 27/04/2016 - RODRIGO SANTANA
   * crud das bebidas.
   * CONTROLLER BEBIDA
   */
  .controller('BebidaCtrl', function ($scope, $ionicLoading, WebserviceBebida, $ionicModal, $ionicPopup) {

    $ionicLoading.show();

    // BUSCAR OS TIPOS DE BEBIDAS
    $scope.listarTipoBebida = WebserviceBebida.buscarTipoBebida(function(retorno) {
      $ionicLoading.hide();
    });

    // BUSCAR AS MARCAS DA BEBIDA
    $scope.listarMarcas = WebserviceBebida.buscarMarcas(function(retorno) {
      $ionicLoading.hide();
    });


    // ION-REFRESH - ATUALIZAR LISTA
    $scope.atualizarLista = function() {
      $scope.listarBebida = WebserviceBebida.buscarBebida(function(){
        $scope.$broadcast('scroll.refreshComplete');
        console.log("Finaliza Refresh")
      });
    };

    // MODAL
    $scope.valoresModal = {};
    $scope.isIncluir = true;

    $ionicModal.fromTemplateUrl('modal.html', function($ionicModal) {
      $scope.modal = $ionicModal;
    }, {
      scope: $scope,
      animation: 'slide-in-up'
    });

    $scope.abrirModal = function(item) {

      if(item){
        $scope.isIncluir = false;
        $scope.valoresModal = {
          'id_tipo_bebida' : item.id_tipo_bebida,
          'nom_tipo_bebida' : item.nom_tipo_bebida
        };
      }else{
        $scope.isIncluir = true;
      }

      $scope.modal.show();
    };

    $scope.fecharModal = function() {
      $scope.modal.hide();
    };


    // EDITANDO O DADO
    $scope.editar = function() {
      $ionicLoading.show();

      var tipo_bebida = new WebserviceTipoBebida();
      tipo_bebida.nome = $scope.valoresModal.nom_tipo_bebida;
      tipo_bebida.$alterarTipoBebida({'id' : $scope.valoresModal.id_tipo_bebida}, function(data) {
        $scope.listaTipoBebida = WebserviceTipoBebida.buscaTipoBebida();
        $scope.valoresModal = {};
        $ionicLoading.hide();
      });
      $scope.modal.hide();
    };

    // INCLUSÃO DE DADOS PARA O WEBSERVICE
    $scope.incluir = function() {
      $ionicLoading.show();

      var bebida = new WebserviceBebida();
      bebida.id_tipo_bebida = $scope.valoresModal.id_tipo_bebida;
      bebida.id_marca = $scope.valoresModal.id_marca;
      bebida.titulo = $scope.valoresModal.titulo;
      bebida.subtitulo = $scope.valoresModal.subtitulo;
      bebida.descricao = $scope.valoresModal.descricao;
      bebida.tamanho = $scope.valoresModal.tamanho;

      bebida.$inserirBebida(function(data) {
        $scope.listarTipoBebida = WebserviceBebida.buscarTipoBebida();
        $scope.valoresModal = {};
        $ionicLoading.hide();
      });
      $scope.modal.hide();
    };

    // CONFIRMANDO POP-UP E EXCLUINDO O DADO
    $scope.excluir = function(item, index){

      var popUp = $ionicPopup.confirm({
        title: 'Confirmação',
        template: 'Você tem certeza que deseja excluir esse registro?',
        okText: 'SIM',
        cancelText: 'NÃO'
      });

      popUp.then(function(botaoSim) {
        if(botaoSim) {
          $ionicLoading.show();
          WebserviceTipoBebida.deletarTipoBebida({'id' : item.id_tipo_bebida}, function(){
            $scope.listaTipoBebida.splice(index, 1);
            $ionicLoading.hide();
          });
        } else {
          console.log("Foi clicado no botão NÃO");
        }
      });
    };
  })
  //FIM BEBIDA

  /*
   * DATA 26/04/2016 - WILLIAM SANTCLER
   * crud das marcas. Ex.: skol, kaiser, sol...
   * CONTROLLER MARCA
   */
  .controller('MarcaBebidaCtrl', function ($scope, $ionicLoading,WebserviceMarca, $ionicModal, $ionicPopup) {

    $ionicLoading.show();

    // BUSCA AS MARCAS DO WEBSERVICE
    $scope.listarMarcas = WebserviceMarca.buscarMarcas(function(retorno) {
        $ionicLoading.hide();
    });


    // ION-REFRESH - ATUALIZAR LISTA
    $scope.atualizarLista = function() {
      $scope.listarMarcas = WebserviceMarca.buscarMarcas(function(){
        $scope.$broadcast('scroll.refreshComplete');
        console.log("Finaliza Refresh")
      });
    };

    // MODAL
    $scope.valoresModal = {};
    $scope.isIncluir = true;

    $ionicModal.fromTemplateUrl('modal.html', function($ionicModal) {
      $scope.modal = $ionicModal;
    }, {
      scope: $scope,
      animation: 'slide-in-up'
    });

    $scope.abrirModal = function(item) {

      if(item){
        $scope.isIncluir = false;
        $scope.valoresModal = {
          'id_marca' : item.id_marca,
          'nom_marca' : item.nom_marca
        };
      }else{
        $scope.isIncluir = true;
      }

      $scope.modal.show();
    };

    $scope.fecharModal = function() {
      $scope.modal.hide();
    };


    // EDITANDO O DADO
    $scope.editar = function() {
      $ionicLoading.show();

      var marca = new WebserviceMarca();
      marca.nome = $scope.valoresModal.nom_marca;
      marca.$alterarMarca({'id' : $scope.valoresModal.id_marca}, function(data) {
        $scope.listarMarcas = WebserviceMarca.buscarMarcas();
        $scope.valoresModal = {};
        $ionicLoading.hide();
      });
      $scope.modal.hide();
    };

    // INCLUSÃO DE DADOS PARA O WEBSERVICE
    $scope.incluir = function() {
      $ionicLoading.show();

      var marca = new WebserviceMarca();
      marca.nome = $scope.valoresModal.nom_marca;
      marca.$inserirMarca(function(data) {
        $scope.listarMarcas = WebserviceMarca.buscarMarcas();
        $scope.valoresModal = {};
        $ionicLoading.hide();
      });
      $scope.modal.hide();
    };

    // CONFIRMANDO POP-UP E EXCLUINDO O DADO
    $scope.excluir = function(item, index){

      var popUp = $ionicPopup.confirm({
       title: 'Confirmação',
       template: 'Você tem certeza que deseja excluir esse registro?',
       okText: 'SIM',
       cancelText: 'NÃO'
      });

      popUp.then(function(botaoSim) {
        if(botaoSim) {
          $ionicLoading.show();
          WebserviceMarca.deletarMarca({'id' : item.id_marca}, function(){
            $scope.listarMarcas.splice(index, 1);
            $ionicLoading.hide();
          });
        } else {
          console.log("Foi clicado no botão NÃO");
        }
      });
    };
  })
  //FIM MARCA

  /*
  * DATA 26/04/2016 - RODRIGO SANTANA
  * crud dos tipos de bebidas. Ex.: refrigerante, cerveja...
  * CONTROLLER TIPO_BEBIDA
  */
  .controller('TipoBebidaCtrl', function ($scope, $ionicLoading, WebserviceTipoBebida, $ionicModal, $ionicPopup) {

    $ionicLoading.show();

    // BUSCA OS TIPOS DO WEBSERVICE
    $scope.listaTipoBebida = WebserviceTipoBebida.buscaTipoBebida(function(retorno) {
        $ionicLoading.hide();
    });


    // ION-REFRESH - ATUALIZAR LISTA
    $scope.atualizarLista = function() {
      $scope.listaTipoBebida = WebserviceTipoBebida.buscaTipoBebida(function(){
        $scope.$broadcast('scroll.refreshComplete');
        console.log("Finaliza Refresh")
      });
    };

    // MODAL
    $scope.valoresModal = {};
    $scope.isIncluir = true;

    $ionicModal.fromTemplateUrl('modal.html', function($ionicModal) {
      $scope.modal = $ionicModal;
    }, {
      scope: $scope,
      animation: 'slide-in-up'
    });

    $scope.abrirModal = function(item) {

      if(item){
        $scope.isIncluir = false;
        $scope.valoresModal = {
          'id_tipo_bebida' : item.id_tipo_bebida,
          'nom_tipo_bebida' : item.nom_tipo_bebida
        };
      }else{
        $scope.isIncluir = true;
      }

      $scope.modal.show();
    };

    $scope.fecharModal = function() {
      $scope.modal.hide();
    };


    // EDITANDO O DADO
    $scope.editar = function() {
      $ionicLoading.show();

      var tipo_bebida = new WebserviceTipoBebida();
      tipo_bebida.nome = $scope.valoresModal.nom_tipo_bebida;
      tipo_bebida.$alterarTipoBebida({'id' : $scope.valoresModal.id_tipo_bebida}, function(data) {
        $scope.listaTipoBebida = WebserviceTipoBebida.buscaTipoBebida();
        $scope.valoresModal = {};
        $ionicLoading.hide();
      });
      $scope.modal.hide();
    };

    // INCLUSÃO DE DADOS PARA O WEBSERVICE
    $scope.incluir = function() {
      $ionicLoading.show();

      var tipoBebida = new WebserviceTipoBebida();
      tipoBebida.nome = $scope.valoresModal.nom_tipo_bebida;
      tipoBebida.$inserirTipoBebida(function(data) {
        $scope.listaTipoBebida = WebserviceTipoBebida.buscaTipoBebida();
        $scope.valoresModal = {};
        $ionicLoading.hide();
      });
      $scope.modal.hide();
    };

    // CONFIRMANDO POP-UP E EXCLUINDO O DADO
    $scope.excluir = function(item, index){

      var popUp = $ionicPopup.confirm({
       title: 'Confirmação',
       template: 'Você tem certeza que deseja excluir esse registro?',
       okText: 'SIM',
       cancelText: 'NÃO'
      });

      popUp.then(function(botaoSim) {
        if(botaoSim) {
          $ionicLoading.show();
          WebserviceTipoBebida.deletarTipoBebida({'id' : item.id_tipo_bebida}, function(){
            $scope.listaTipoBebida.splice(index, 1);
            $ionicLoading.hide();
          });
        } else {
          console.log("Foi clicado no botão NÃO");
        }
      });
    };
  })
  //FIM TIPO_BEBIDA





  .controller('OfertasCtrl', function ($scope, OfertaFactory, $ionicSlideBoxDelegate, $cordovaSocialSharing) {

    //Slide Banner
    $scope.navSlide = function (index) {
      $ionicSlideBoxDelegate.slide(index, 500);
    };

    // Busca Ofertas
    $scope.listaPromocoes = OfertaFactory.listaOfertas();

    // Compartilhar
    $scope.compartilhar = function () {
      $cordovaSocialSharing.share("App para buscar sua bebida mais próxima e mais barata!", "Bora Beber?", "http://sd.keepcalm-o-matic.co.uk/i/keep-calm-and-vamo-ou-bora-beber-2.png", "http://www.borabeber.com");
      console.log("chamando o compartilhar");
    }
  })

  .controller('ComercianteCtrl', function ($scope) {
  })

  .controller('MapaCtrl', function ($scope, $ionicLoading, $cordovaGeolocation) {

    var options = {timeout: 10000, enableHighAccuracy: true};

    $cordovaGeolocation.getCurrentPosition(options).then(function (position) {

      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      var mapOptions = {
        center: latLng,
        zoom: 17,
        disableDefaultUI:true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      google.maps.event.addListenerOnce($scope.map, 'idle', function(){

        var marker = new google.maps.Marker({
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          position: latLng
        });

        var infoWindow = new google.maps.InfoWindow({
          content: "Here I am!"
        });

        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
        });

      });

    }, function (error) {
      console.log("Could not get location");
    });
  })
